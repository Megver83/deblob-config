#!/bin/bash
# Look for CONFIG lines which try to enable blobs
# Copyright (C) David P. <megver83@parabola.nu>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

version=$1
args=${@#$1}
path=${0%/*}

usage() {
cat <<EOM
Usage: ${0##*/} [VERSION] [FILE1] [FILE2] ...
Where VERSION should be the kernel version and FILE the config file(s).
Example: ${0##*/} 4.14 /usr/src/linux-4.14/.config

To see a list of available versions, look at the files under the $path/versions
directory.

If the \$RESET environment variable is set, then the sed expression to reset
CONFIG_LOCALVERSION and CONFIG_DEFAULT_HOSTNAME variables of the specified
config files will be printed and applied. Also CONFIG_NFS_V4_1_IMPLEMENTATION_ID_DOMAIN
will be set to "linux-libre.fsfla.org" and CONFIG_LOCALVERSION will be set to
what \$RESET was set.

When RESET is empty then it is considered to have an empty CONFIG_LOCALVERSION.
And when RESET='none', then we do not touch CONFIG_LOCALVERSION.

If the \$FIRMWARE variable is set, then that value will be the same one
for CONFIG_EXTRA_FIRMWARE. If you also want CONFIG_EXTRA_FIRMWARE_DIR, just
do as following:

  FIRMWARE='some_firmware.bin other_firm.elf:firmware'

Where 'firmware' is the firmware directory. When this is empty, CONFIG_EXTRA_FIRMWARE_DIR
gets removed. E.g.

  FIRMWARE='some_firmware.bin other_firm.elf'

When FIRMWARE is empty, then CONFIG_EXTRA_FIRMWARE is left unset and CONFIG_EXTRA_FIRMWARE_DIR
gets removed. However, when FIRMWARE='none' then the script does not touch these lines.
EOM
}

err() {
    printf '%sError:%s %s\n' \
            "$(tput setaf 1)" \
            "$(tput sgr0)" \
            "$1"
    false
}

check() {
    if [[ "$version" = none ]]; then
        echo "Running with reset expressions only"
    elif [ ! -f $path/versions/$version ]; then
        err "Version $version not available"
    fi

    if [[ $args != "" ]]; then
        for f in $args; do
            [ -f $f ] || err "$f does not exist"
        done
    else
        err 'No config files provided'
    fi
}

deblob() {
    local configs=$(grep -v ^# $path/versions/$version | awk '{print $1}')
    local i
    for i in $configs; do
        sed -i \
            -e "s|$i=.*|# $i is not set|" \
            -e "$reset" \
            -e "$firmware" \
            $1
        sleep 0.2
    done
}

load(){
local num=${#1}
local i
for i in $(seq $num); do
    space+=" "
done

while true
do
    printf "%s                  \r" "$space"
    printf "[ running ] $1\r"
    sleep 0.2
    printf "[ running ] $1 .\r"
    sleep 0.2
    printf "[ running ] $1 ..\r"
    sleep 0.2
    printf "[ running ] $1 ...\r"
    sleep 0.2
    printf "[ running ] $1 ....\r"
    sleep 0.2
    printf "[ running ] $1 .....\r"
    sleep 0.2
done
}

main(){
    check
    if [[ "$version" = none ]]; then
        local i
        for i in $@; do
            load "Modifying $i" &
            LOG=$(mktemp)
            sed -i \
                -e "$reset" \
                -e "$firmware" \
                $i &> $LOG && \
                printf "[   OK   ] \n" || \
                (printf "[ FAILED ] \n"
                err "Could not modify $i") || \
                cat $LOG
            kill $!
            wait $! 2>/dev/null || true
            rm $LOG
        done
        echo "No deblobbing for configs for now"
    else
        local i
        for i in $@; do
            load "Modifying $i" &
            LOG=$(mktemp)
            deblob $i &> $LOG && \
                printf "[   OK   ] \n" || \
                (printf "[ FAILED ] \n"
                err "Could not modify $i") || \
                cat $LOG
            kill $!
            wait $! 2>/dev/null || true
            rm $LOG
        done
    fi
}

if [[ $# -lt 1 ]]; then
    usage
else
    if [[ $RESET = "" ]]; then
        echo "RESET sedexp"
        export reset="s|^CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\"\"|;s|^CONFIG_DEFAULT_HOSTNAME=.*|CONFIG_DEFAULT_HOSTNAME=\"parabola\"|;s|^CONFIG_NFS_V4_1_IMPLEMENTATION_ID_DOMAIN=.*|CONFIG_NFS_V4_1_IMPLEMENTATION_ID_DOMAIN=\"linux-libre.fsfla.org\"|"
        if [[ $version > 4.17 ]]; then
          export reset="$reset;s|# CONFIG_LOCALVERSION_AUTO is not set|CONFIG_LOCALVERSION_AUTO=y|"
        fi
        echo "$reset"
        echo
    elif ! [[ $RESET = "none" ]]; then
        echo "RESET sedexp"
        export reset="s|^CONFIG_LOCALVERSION=.*|CONFIG_LOCALVERSION=\""$RESET"\"|;s|^CONFIG_DEFAULT_HOSTNAME=.*|CONFIG_DEFAULT_HOSTNAME=\"parabola\"|;s|^CONFIG_NFS_V4_1_IMPLEMENTATION_ID_DOMAIN=.*|CONFIG_NFS_V4_1_IMPLEMENTATION_ID_DOMAIN=\"linux-libre.fsfla.org\"|"
        if [[ $version > 4.17 ]]; then
          export reset="$reset;s|# CONFIG_LOCALVERSION_AUTO is not set|CONFIG_LOCALVERSION_AUTO=y|"
        fi
        echo "$reset"
        echo
    fi

    if [[ $FIRMWARE = "" ]]; then
        echo "FIRMWARE sedexp"
        export firmware="/^CONFIG_EXTRA_FIRMWARE_DIR/d;s|^CONFIG_EXTRA_FIRMWARE=.*|CONFIG_EXTRA_FIRMWARE=\"\"|"
        echo "$firmware"
        echo
    elif ! [[ $FIRMWARE = "none" ]]; then
        echo $FIRMWARE | grep : && \
        extra_firmware=$(echo $FIRMWARE | cut -d ":" -f1) && \
        extra_firmware_dir=$(echo $FIRMWARE | cut -d ":" -f2) || \
        extra_firmware=$FIRMWARE

        echo "FIRMWARE sedexp"
        if ! [[ $extra_firmware = "" ]]; then
            if ! [[ $extra_firmware_dir = "" ]]; then
                export firmware="s|^CONFIG_EXTRA_FIRMWARE=.*|CONFIG_EXTRA_FIRMWARE=\""$extra_firmware"\"|;s|^CONFIG_EXTRA_FIRMWARE_DIR=.*|CONFIG_EXTRA_FIRMWARE_DIR=\""$extra_firmware_dir"\"|"
            else
                export firmware="s|^CONFIG_EXTRA_FIRMWARE=.*|CONFIG_EXTRA_FIRMWARE=\""$extra_firmware"\"|;/CONFIG_EXTRA_FIRMWARE_DIR/d"
            fi
            echo "$firmware"
            echo
        fi
    fi
    main $args
fi
