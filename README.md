## FAQ
### What is this?
A README file

### No seriously, what's deblob-config?
It is a collection of scripts which edit, using `sed`, a specified kernel configuration file for the version you choose,
no matter the architecture. It disables CONFIG lines known to work only with blobs, making the compilation a bit slower
and showing annoying "/\*(DEBLOBBED)\*/" messages in the kernel log (yes, the kernel log from `dmesg`, **besides** the build log).

Most of this CONFIG lines are taken from [this LibrePlanet wiki page](http://libreplanet.org/wiki/LinuxLibre:Devices_that_require_non-free_firmware),
but we do also add newer ones according to our build logs. If there's a blobbed config which is not here, it's likely
to be disabled by Arch (so Parabola disables it too). In that case, please get in touch with us to we can add it here.

### Is it necessary to maintain this? Why does not Parabola manually changes their kernel config files?
We want to diverge as little as possible from Arch kernel configs, which are made for the blobbed Linux kernel. It is easier
to "deblob" their configs (I say "deblob" with "" because it's not real deblobbing, just disabling blob's configs) than
copy-pasting their changes or even applying them as patches, which may conflict with our changes.

### Why CONFIG_XXX is not included?
Probably Arch also disables such config line, making the "/\*(DEBLOBBED)\*/" message to hide from our logs.

## Contributing
If you wish to do a request, report a bug, send feedback, etc. you can contact us on our usual channels:

 * https://labs.parabola.nu
 * irc://freenode.net/#parabola
 * mailto:dev@lists.parabola.nu
